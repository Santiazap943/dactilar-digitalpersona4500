# Dactilar Digital Persona 4500
Dactilar Digital Persona 4500 is a desktop application to manage fingerprint reader.

## Getting started
### Prerequisites:

| Item| Specifications|
|--|--|
| Operative System | Windows |
| IDE | [NetBeans](https://netbeans.org/downloads/8.2/rc/) |
| Language | [Java](https://www.java.com/es/download/) |
| JDK| [8](https://www.oracle.com/java/technologies/javase/javase-jdk8-downloads.html) |
| Database| [MySQL](https://dev.mysql.com/downloads/mysql/)|
| Fingerprint Reader | Digital Persona 4500 Drivers |


#### Libs

- AbsoluteLayout
- BeansBinding 1.2.1
- [MySQL Connector / J 5.1.23](https://cdn.mysql.com/archives/mysql-connector-java-5.1/mysql-connector-java-5.1.23.zip)

  *One Touch for Windows SDK: [Link Suggested](https://github.com/Eliezer090/JavaLibsPersonal/blob/master/JavaLibs.rar)*

- dpfpenrollment
- dpfpverification
- dpotapi
- dpotjni

#### Database config

1. Copy from /src/Persistencia/mysql.properties.example to /src/Persistencia/mysql.properties 
2. Complete with database credencials

### [](#installation)Installation

1. Download and pull code from repository

```
C:\path\
$ mkdir my-java-application

C:\path\
$ cd my-java-application\

C:\path\my-java-application\
$ git init

C:\path\my-java-application\
$ git remote add origin https://gitlab.com/Santiazap943/dactilar-digitalpersona4500

C:\path\my-java-application\
$ git pull origin master
```

2. Import libs

```
C:\path\my-java-application\
$ mkdir dist

C:\path\my-java-application\
$ cd dist

C:\path\my-java-application\dist\
$ mkdir lib

C:\path\my-java-application\dist\
$ cd lib

C:\path\my-java-application\dist\lib\
$ xcopy \path-of-downloaded-libs\* .

```

3. Run project

## Contact


| Role| Name| Email |
|--|--|--|
| Owner | Juan Rodriguez| 
| Owner| Santiago Aza | 
| Developer| David Tabla | [d2davidtb@gmail.com](mailto:d2davidtb@gmail.com)
| Developer| Camilo Sandoval | [camilo.sandoval.ad@gmail.com](mailto:camilo.sandoval.ad@gmail.com)

by [ITI](http://itiud.org) research group of [Universidad Distrital Francisco José de Caldas](https://www.udistrital.edu.co/en/index)

